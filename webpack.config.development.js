var config = require('./webpack.config.base.js');

const webpack = require('webpack')
const BundleTracker = require('webpack-bundle-tracker')


config.plugins = config.plugins.concat([
  new BundleTracker({filename: './webpack-stats.json'}),
  new webpack.SourceMapDevToolPlugin({filename: '[file].map'}),
])


module.exports = config
