import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  constructor(public translate: TranslateService) { }

  ngOnInit() {
    this.translations();
  }

  translations() {
    // this language will be used as a fallback when a translation isn't found in the current language
    this.translate.setDefaultLang('en');
    // Current browser language
    const language = window.navigator.language;
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this.translate.use(language);
  }

}
