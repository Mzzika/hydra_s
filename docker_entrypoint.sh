#!/usr/bin/env bash


set -e


if [ $1 = 'build' ] ; then
    # Build static files
    exec yarn run build

elif [ $1 = 'build_production' ] ; then
    # Build static files for production
    exec yarn run build-production

elif [ $1 = 'watch' ] ; then
    # Run webpack development server
    exec yarn run watch

else
    exec "$@"
fi