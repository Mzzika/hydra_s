# Hydra

### What is this repository for? ###

Hydra is the frontend for Pluto project, a search proxy for federated search ("Intelligence Cloud")

It uses the latest nodejs, yarn as package manager and webpack as module bundler.

It's required to create a shared volume while running in docker-compose with Pluto, in order to share the produced
static assets files with the nginx container, that will finally serve the web application.

### How do I get set up? ###

* Build docker dev image

    ```
    ./hydra.sh build
    ```

* Build static files.

    ```
    ./hydra.sh build_static
    ```

* Build production static files.

    ```
    ./hydra.sh build_static_production
    ```

* Run dev image webpack watch process, detaching and running in background.

    ```
    ./hydra.sh watch
    ```

* Run dev image and show webpack watch in foreground. CTRL-C will stop the container.

    ```
    ./hydra.sh run
    ```

* Stop dev image

    ```
    ./hydra.sh stop
    ```
    
* Build the project

```
ng build
```

* Run the project

```
ng serve --open
```

* Build for production (will generate dist folder)

```
ng build --prod
```

### Test coverage ###

TODO
