var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')
var config = require('./webpack.config.base.js')

config.output.path = require('path').resolve('./dist/')

config.plugins = config.plugins.concat([
  // Stats file is used to determine static files location
  new BundleTracker({filename: './webpack-stats-prod.json'}),
  // Sets the node env to production
  new webpack.DefinePlugin({'process.env': {'NODE_ENV': JSON.stringify('production')}}),
  // Keeps hashes consistent between compilations
  new webpack.optimize.OccurrenceOrderPlugin()
])

module.exports = config