#!/usr/bin/env bash

set -e
DOCKER_APP_IMAGE_NAME=hydra/hydra:latest
COMMAND=${1}

if [ -z ${COMMAND} ] ; then
    echo 'Missing argument'
    echo '   ./hydra.sh build                      Build docker image'
    echo '   ./hydra.sh build_static        	   Build development static files'
    echo '   ./hydra.sh build_static_production    Build production static files'
    echo '   ./hydra.sh watch_static               Watch static files changes in development mode'
    exit
fi

# Build dev environment
if [ ${COMMAND} = 'build' ] ; then
    docker build -t ${DOCKER_APP_IMAGE_NAME} .

# Allow multiple commands to support intuitive usage
elif [ ${COMMAND} = 'start' -o ${COMMAND} = 'run' ] ; then
    # Run the dev server
    docker run -d -p 4300:80 ${DOCKER_APP_IMAGE_NAME}

elif [ ${COMMAND} = 'build_static' ] ; then
    docker run --rm ${DOCKER_APP_IMAGE_NAME} build

elif [ ${COMMAND} = 'build_static_production' ] ; then
    docker run --rm ${DOCKER_APP_IMAGE_NAME} build_production

elif [ ${COMMAND} = 'watch' ] ; then
    docker run -d -p 4300:80 ${DOCKER_APP_IMAGE_NAME} watch

fi
