const path = require("path")
const webpack = require('webpack')


module.exports = {
  context: __dirname,

  entry: [
      './src/index'
  ],

  output: {
      path: path.resolve('./src/bundles'),
      publicPath: '/static/',
      filename: "[name]-[hash].js"
  },

  plugins: [
      new webpack.HotModuleReplacementPlugin()
  ],

  devtool: 'source-map',

  module: {
    rules: [
        {
            test: /\.ts$/,
            loader: "awesome-typescript-loader"
        }
    ]
  },

  resolve: {
    modules: ['node_modules', 'bower_components'],
    extensions: ['*', '.js', '.ts']
  }

}
